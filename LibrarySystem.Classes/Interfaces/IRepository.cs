﻿using LibrarySystem.Classes.Models;
using LibrarySystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem.Classes.Interfaces
{
    public interface IRepository
    {
        IEnumerable<User> Users { get; }
        bool Authorize(string login, string paasword);
        void Register(User user);
    
        void Take(int id);
        void BringingBack(int id);
        IEnumerable<Book> SearchResultByName(string text);
        IEnumerable<Book> SearchResultByAuthor(string text);
        IEnumerable<Book> GotBooks { get; }
        
    }
}
