﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        [JsonIgnore]
        public string Name { get; set; }
        public string PassportNumber { get; set; }
    
        public Dictionary<Book, DateTime> Picked_books {get;set;}
        public Dictionary <Book,DateTime> Booked_books { get; set; }


    }
}
