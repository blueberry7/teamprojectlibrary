﻿using LibrarySystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem.Classes.Models
{
    public class ForSearching
    {
        public ForSearching()
        {
            Dictionary<Book, DateTime> book_was_taken = new Dictionary<Book, DateTime>();
            if (SearchingBook.Availability == false)
            {
                foreach(var t in GotItems)
                {
                    if (t.gotItem == SearchingBook) book_was_taken.Add(t.gotItem,t.TimeOfGetting.AddDays(30));
                }
                
                book_was_taken = book_was_taken.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
                TimeOfReturning = book_was_taken.First().Value;
            }
        }

        public Book SearchingBook { get; set; }
        
        public DateTime TimeOfReturning { get; set; }
        public List<GotItem> GotItems {get;set;}

        







        
    }
}
