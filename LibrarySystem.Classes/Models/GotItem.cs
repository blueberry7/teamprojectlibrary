﻿using LibrarySystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem.Classes.Models
{
    public class GotItem
    {
        public User User { get; set; }
        public Book gotItem { get; set; }
        public DateTime TimeOfGetting { get; set; }


         
    }
}
