﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibrarySystem.Classes;
using LibrarySystem.Classes.Models;

namespace LibrarySystem.Models
{
    public class Book
    {

        public int Id { get; set; }
        [JsonIgnore]
        public string Name { get; set; }
        public string Author_Surname { get; set; }
        public string Author_Name { get; set; }
        public string Author_Otchevstvo { get; set; }
        public string PublishingHouse { get; set; }
        public int Count { get; set; }
        public DateTime Time_of_first_returning {get;set;}
        public bool Availability { get;set;}
        public string Country { get; set; }
        public string ImageLink { get; set; }
        public string Language { get; set; }
        public string Link { get; set; }
        public int pages { get; set; }
        public  string Year { get; set; }
        public  Book()
        {
            if (Count == 0)
            {
                Availability = false;
              
             }
            else
            {
                Availability = true;
            }
            string author = Author_Surname + " " + Author_Name + " " + Author_Otchevstvo;
        }
        



    }
}
