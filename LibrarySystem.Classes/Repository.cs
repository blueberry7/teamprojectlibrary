﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibrarySystem.Classes.Interfaces;
using LibrarySystem.Classes.Models;
using LibrarySystem.Models;
using Newtonsoft.Json;

namespace LibrarySystem.Classes
{
    internal class Repository : IRepository
    {
        public class GeneralData
        {
            public List<User> users { get; set; }
            public List<Book> books { get; set; }
        }
        private const string DataFolder = "Data";
        private const string FileName = "Information.json";
        private GeneralData data;
        private User _authorizedUser;

        private List<GotItem> gotBooks => new List<GotItem>();
        
        public IEnumerable<User> Users => data.users;


        public IEnumerable<Book> GotBooks => picked_books;    
        public Repository()
        {


            try
            {
                using (var sr = new StreamReader(Path.Combine(DataFolder, FileName)))
                {
                    using (var jsonReader = new JsonTextReader(sr))
                    {
                        var serializer = new JsonSerializer();
                        data = serializer.Deserialize<GeneralData>(jsonReader);
                    }
                }

            }
            catch
            {
                data = new GeneralData
                {
                    users = new List<User>(),
                    books = new List<Book>()

                };
            }
        }
        
        public void Register(User user)
        {
            user.Id = data.users.Count > 0 ? data.users.Max(u => u.Id) + 1 : 1;
            data.users.Add(user);
            Save();
        }
        public void Save()
        {
            /* if (!Directory.Exists(DataFolder))
             {
                 Directory.CreateDirectory(DataFolder);
             }*/
            using (var sw = new StreamWriter(Path.Combine(DataFolder, FileName)))
            {
                using (var jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.Formatting = Formatting.Indented;
                    var serializer = new JsonSerializer();
                    serializer.Serialize(jsonWriter, data);
                }
            }
        }
            
                
        
         


        List<Book> picked_books = new List<Book>();
        public bool Authorize(string login, string password)
        {
            var user = data.users.FirstOrDefault(u => u.Login == login && u.Password == PasswordHelper.GetHash(password));
            if (user != null)
            {
                _authorizedUser = user;
                if(_authorizedUser.Picked_books!=null)
                    picked_books =_authorizedUser.Picked_books.Keys.ToList();
                
                return true;
            }
            return false;
        }

        public IEnumerable<Book> SearchResultByName (string text)
        {
            List<Book> result = new List<Book>();
            DateTime current_time = DateTime.Now;
            foreach( var book in data.books)
            {
                if (text == book.Name)
                {
                    if (book.Availability == false)
                    {
                        ForSearching forSearching = new ForSearching() { SearchingBook = book, GotItems = gotBooks };
                        book.Time_of_first_returning = forSearching.TimeOfReturning;
                        result.Add(book);
                    }
                    result.Add(book);
                }   
            }
            return result;
        }
       
        public IEnumerable<Book> SearchResultByAuthor(string text)
        {
            List<Book> result = new List<Book>();
            DateTime current_time = DateTime.Now;
            foreach (var book in data.books)
            {
                if (text == book.Author_Surname || text==book.Author_Name|| text==book.Author_Otchevstvo)
                {
                    if (book.Availability == false)
                    {
                        ForSearching forSearching = new ForSearching() { SearchingBook = book, GotItems = gotBooks };
                        book.Time_of_first_returning = forSearching.TimeOfReturning;
                        result.Add(book);
                    }
                    result.Add(book);
                }
                    
            }
            return result;


        }

        public void Take(int book_id)
        {
            foreach (var b in data.books)
            {
                if (b.Id == book_id)
                {
                    b.Count--;
                    _authorizedUser.Picked_books.Add(b,DateTime.Now);
                    var gotBook = new GotItem() { User = _authorizedUser, gotItem = b, TimeOfGetting = DateTime.Now };
                    gotBooks.Add(gotBook);


                }
            }
        }
        public void BringingBack(int book_id)
        {
            foreach (var b in data.books)
            {
                if (b.Id == book_id)
                {
                    b.Count++;
                    _authorizedUser.Picked_books.Remove(b);
                    foreach(var t in gotBooks)
                    {
                        if (b.Id == t.gotItem.Id && _authorizedUser == t.User) gotBooks.Remove(t);
                    }
                }
            }
        }

       
        

    }
}

