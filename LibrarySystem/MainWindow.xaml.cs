﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonPickReturnBook_Click(object sender, RoutedEventArgs e)
        {

            var terminalWindow = new TerminalWindow();
            terminalWindow.PickedOrReturned+=PickingOrReturningFinished;
            
            terminalWindow.Show();
            Hide();
        }

        private void ButtonFindBook_Click(object sender, RoutedEventArgs e)
        {
            var searchWindow = new SearchWindow();
            searchWindow.Show();
            Hide();
        }

        private void ButtonPickedBookedBooks_Click(object sender, RoutedEventArgs e)
        {
            var pickedBooksWindow = new PickedBooksWindow();
            pickedBooksWindow.Show();
            Hide();
        }

      
        private void PickingOrReturningFinished()
        {
            Show();
          
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            var loginWindow = new LoginWindow();
            loginWindow.Show();
        }
    }
}
