﻿using LibrarySystem.Classes;
using LibrarySystem.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            
            if (String.IsNullOrWhiteSpace(textBoxEmail.Text)||(String.IsNullOrWhiteSpace(textBoxPassword.Password)))
            {
                MessageBox.Show("Incorrect login/password");
            }
            else
            {
                if(_repo.Authorize(textBoxEmail.Text, textBoxPassword.Password))
            {
                    var mainWindow = new MainWindow();
                    mainWindow.Show();

                    Close();
                }
                else
                    MessageBox.Show("Incorrect login/password");

            }
        }    

        private void ButtonRegister_Click(object sender, RoutedEventArgs e)
        {
            var registrationWindow = new RegistrationWindow();
            registrationWindow.RegistrationFinished += RegistrationWindow_RegistrationFinished;
            registrationWindow.Show();

            Hide();
        }

        private void RegistrationWindow_RegistrationFinished()
        {
            Show(); 
        }
    }
}
