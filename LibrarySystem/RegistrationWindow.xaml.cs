﻿using LibrarySystem.Classes;
using LibrarySystem.Classes.Interfaces;
using LibrarySystem.Classes.Models;
using LibrarySystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();

        public event Action RegistrationFinished;

        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void ButtonRegistration_Click(object sender, RoutedEventArgs e)
        {
            var user = new User();
            if (String.IsNullOrEmpty(textBoxName.Text) || String.IsNullOrEmpty(textBoxEmail.Text) || String.IsNullOrEmpty(textBoxPassport.Text) || String.IsNullOrEmpty(textBoxPassword.Password))
                MessageBox.Show("Insert your data correctly");
            else
            {
                user.Name = textBoxName.Text;
                user.PassportNumber = textBoxPassport.Text;
                user.Login = textBoxEmail.Text;
                user.Password = PasswordHelper.GetHash(textBoxPassword.Password);

            };
            try
            {
                _repo.Register(user);
                RegistrationFinished?.Invoke();
                Close();
            }
            catch
            {
                MessageBox.Show("An error occured trying to save new user");
            }
        }

        private void ButtonCancelRegistration_Click(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();

            this.Close();
        }

       
    }
}
