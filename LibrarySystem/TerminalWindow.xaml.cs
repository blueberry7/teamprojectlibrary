﻿using LibrarySystem.Classes;
using LibrarySystem.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для TerminalWindow.xaml
    /// </summary>
    public partial class TerminalWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();
        public TerminalWindow()
        {
            InitializeComponent();
            
        }
        public event Action PickedOrReturned;
        int book_id;
        
        private void ButtonCancelScanning_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            var mainWindow = new MainWindow();
            mainWindow.Show();
        }

        private void Pick_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(textBoxId.Text))

                MessageBox.Show("Insert Id of your book");
            else
            {
                try
                {
                    book_id = int.Parse(textBoxId.Text);

                    _repo.Take(book_id);
                    PickedOrReturned?.Invoke();
                    this.Close();
                    MessageBox.Show("The book is picked");
                    
                }
                catch
                {
                    MessageBox.Show("Invalid value");
                }
            } 
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxId.Text))

                MessageBox.Show("Insert Id of your book");
            else
            {
                try
                {
                    book_id = int.Parse(textBoxId.Text);
                    _repo.BringingBack(book_id);
                    PickedOrReturned?.Invoke();
                    this.Close();
                    MessageBox.Show("The book is returned");
                   
                }
                catch
                {
                    MessageBox.Show("Invalid value");
                }
            }
            
        }
    }
}
