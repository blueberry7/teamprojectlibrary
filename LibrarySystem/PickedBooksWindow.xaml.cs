﻿using LibrarySystem.Classes;
using LibrarySystem.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для PickedBooksWindow.xaml
    /// </summary>
    public partial class PickedBooksWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();

        public PickedBooksWindow()
        {
            InitializeComponent();
            Show_Picked_Books();
        }


        private void Show_Picked_Books()
        {
            if (_repo.GotBooks != null) pickedBooks.ItemsSource = _repo.GotBooks;
            else MessageBox.Show("No picked books");
            
        }
        
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            var mainWindow = new MainWindow();
            mainWindow.Show();
        }
    }
}
