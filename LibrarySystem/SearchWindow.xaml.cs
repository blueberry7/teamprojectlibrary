﻿using LibrarySystem.Classes;
using LibrarySystem.Classes.Interfaces;
using LibrarySystem.Classes.Models;
using LibrarySystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Логика взаимодействия для SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {

        public SearchWindow()
        {
            InitializeComponent();
            List<Book> searchBookResult = new List<Book>();
            //string bookNameTextBlock = searchBookResult.Book.Name;
        }

        private void ButtonSearchByName_Click(object sender, RoutedEventArgs e)
        {
            //searchResult = SearchResultByName(searchByName.Text);
        }

        private void ButtonSearchByAuthor_Click(object sender, RoutedEventArgs e)
        {
            //searchResult = SearchResultByAuthor(searchByName.Text);
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            var mainWindow = new MainWindow();
            mainWindow.Show();
        }

       
    }
}
